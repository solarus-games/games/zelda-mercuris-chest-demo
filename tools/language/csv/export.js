const fs = require("fs");

const solarusProjectPath = "../../../data";
const languagePath = `${solarusProjectPath}/languages`;

const getProperty = (components) => {
  return {
    name: components[0].trim(),
    value: components[1].trim().replace(/"(.+)",/, "$1"),
  };
};

const getDialogsFromFile = (baseLanguage) => {
  const dialogPath = `${languagePath}/${baseLanguage}/text/dialogs.dat`;
  const data = fs.readFileSync(dialogPath, "utf-8");
  const lines = data.split("\n");

  // States
  let isInDialog = false;
  let currentDialog = {};
  let isInText = false;

  const dialogs = [];

  // Start reading dialogs
  for (const line of lines) {
    const trimmedLine = line.replace("\r", "").trim();

    // Start one dialog object
    if (trimmedLine == "dialog{") {
      isInDialog = true;
      currentDialog = {
        textLines: [],
      };
      continue;
    }

    // End one dialog object
    if (trimmedLine == "}" && isInDialog) {
      isInDialog = false;
      dialogs.push(currentDialog);
      continue;
    }

    // Read lines of a dialog object
    if (isInText) {
      if (trimmedLine == "]],") {
        isInText = false;
        continue;
      }
      const textLine = trimmedLine;
      currentDialog.textLines = [...currentDialog.textLines, textLine];
    } else if (isInDialog) {
      const property = getProperty(trimmedLine.split("="));

      switch (property.name) {
        case "id":
          currentDialog.id = property.value;
          break;
        case "icon":
          currentDialog.icon = property.value;
          break;
        case "skip":
          currentDialog.skip = property.value;
          break;
        case "text":
          isInText = true;
        default:
          break;
      }
    }
  }

  return dialogs;
};

const writeCsvDialogs = (dialogs) => {
  const lines = dialogs.map((dialog) => {
    const escapedLines = dialog.textLines.map((line) => {
      return line.replace(/\"/, '""');
    });
    return `\"${dialog.id}\",\"${escapedLines.join("\n")}\"`;
  });

  const stream = fs.createWriteStream("dialogs.csv");
  for (const line of lines) {
    stream.write(line + "\n");
  }
  stream.end();
};

try {
  const baseLanguage = process.argv[2] ?? "fr";
  const dialogs = getDialogsFromFile(baseLanguage);
  writeCsvDialogs(dialogs);
} catch (error) {
  console.error(error);
}
