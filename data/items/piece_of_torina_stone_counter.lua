local item = ...
local game = item:get_game()

function item:on_started()
  
  self:set_savegame_variable("item_status_piece_of_torina_stone_counter")
  self:set_assignable(false)
  self:set_amount_savegame_variable("item_status_piece_of_torina_stone_amount")
  self:set_max_amount(5)
end

