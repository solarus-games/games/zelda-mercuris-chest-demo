-- Reaction sprite like an exclamation mark or a question mark to put near an NPC.
-- Gets destroyed automatically after a small delay.

local entity = ...
local game = entity:get_game()
local map = entity:get_map()

function entity:on_enabled()
  sol.timer.start(entity, 1000, function()
    entity:remove()
  end)
end
