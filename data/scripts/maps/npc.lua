local npc = {}

local products = {
  -- buy or sell
  heart_one = {
    price = 5,
    treasure = "heart",
    variant = 1,
    sell_action = "heart",
    sell_amount = 4,
  },
  -- only for sell
  heart_three = {
    price = 15,
    treasure = "heart",
    variant = 2,
    sell_action = "heart",
    sell_amount = 12,
  },
  -- only for sell
  heart_seven = {
    price = 35,
    treasure = "heart",
    variant = 3,
    sell_action = "heart",
    sell_amount = 12,
  },
  -- buy or sell
  magic_small = {
    price = 5,
    treasure = "magic_flask",
    variant = 1,
    sell_action = "magic",
    sell_amount = 6,
    item_required = "magic_bar",
  },
  -- buy or sell
  magic_big = {
    price = 35,
    treasure = "magic_flask",
    variant = 2,
    sell_action = "magic",
    sell_amount = 42,
    item_required = "magic_bar",
  },
  bomb_one = {
    price = 10,
    treasure = "bomb",
    variant = 1,
    sell_action = "bomb",
    sell_amount = 1,
    item_required = "bomb_bag",
  },
  bomb_three = {
    price = 30,
    treasure = "bomb",
    variant = 2,
    sell_action = "bomb",
    sell_amount = 3,
    item_required = "bomb_bag",
  },
  bomb_eight = {
    price = 80,
    treasure = "bomb",
    variant = 3,
    sell_action = "bomb",
    sell_amount = 8,
    item_required = "bomb_bag",
  },
  croissant_one = {
    price = 30,
    treasure = "croissant",
    variant = 1,
  },
  pain_au_chocolat_one = {
    price = 15,
    treasure = "pain_au_chocolat",
    variant = 1,
  },
  apple_one = {
    price = 10,
    treasure = "apple",
    variant = 1,
  },
  fairy = {
    price = 50,
    treasure = "fairy",
    variant = 1,
  }
}

local sell_actions = {
  -- Removes the amount of life for money
  -- But the selling action won't kill the player and return false
  heart = function(amount)
    local game = sol.main.game
    if game:get_life() <= amount then
      return false
    end
    game:remove_life(amount)
    return true
  end,
  
  -- Removes magic for money
  magic = function(amount)
    local game = sol.main.game
    if game:get_magic() < amount then
      return false
    end
    game:remove_magic(amount)
    return true
  end,

  bomb = function(amount)
    local game = sol.main.game
    if game:get_item("bombs_counter"):get_amount() < amount then
      return false
    end
    game:get_item("bombs_counter"):remove_amount(amount)
    return true
  end
}

local function can_buy(game, product_name)
  local ability_required = products[product_name].ability_required
  local item_required = products[product_name].item_required

  if ability_required ~= nil then
    if not game:has_ability(ability_required, 1) then
      return false
    end
  elseif item_required ~= nil then
    if not game:has_item(item_required, 1) then
      return false
    end
  end

  return true
end

local function get_required_label(product_name)
  local ability_required = products[product_name].ability_required
  local item_required = products[product_name].item_required

  if ability_required ~= nil then
    return ability_required
  elseif item_required ~= nil then
    return item_required
  end

  return nil
end

function npc:setup_seller(npc, props)
  local map = npc:get_map()
  local game = map:get_game()
  local hero = map:get_hero()

  npc.on_interaction = function()
    local price = products[props.product].price
    local treasure = products[props.product].treasure
    local variant = products[props.product].variant

    game:start_dialog("_trading.sell.products." .. props.product, function()
      game:start_dialog("_trading.sell.question", price, function(answer)
        if answer == 1 then
          if game:get_money() >= price then
            -- do buy
            if can_buy(game, props.product) then
              game:remove_money(price)
              hero:start_treasure(treasure, variant or 1)
            else
              sol.audio.play_sound("wrong")
              game:start_dialog("_trading.required." .. get_required_label(props.product))
            end
          else
            -- not enough money
            sol.audio.play_sound("wrong")
            game:start_dialog("_trading.not_enough_money")
          end
        end
      end)
    end)
  end
end

function npc:setup_buyer(npc, props)
  local map = npc:get_map()
  local game = map:get_game()

  npc.on_interaction = function()
    local price = products[props.product].price
    local sell_action = products[props.product].sell_action
    local sell_amount = products[props.product].sell_amount

    game:start_dialog("_trading.buy.products." .. props.product, function()
      game:start_dialog("_trading.buy.question", price, function(answer)
        if answer == 1 then
          local do_selling_action = sell_actions[sell_action]
          if do_selling_action == nil then
            print("Unable to find selling action " .. sell_action)
          else
            local success = do_selling_action(sell_amount)
            if success then
              -- do sell
              game:add_money(price)
            else
              -- not enough of resource
              sol.audio.play_sound("wrong")
              game:start_dialog("_trading.not_enough_resource." .. props.product)
            end
          end
        end
      end)
    end)
  end
end

function npc:init_talk_with_torin(name, map_name)
  local game = sol.main.game
  local map = game:get_map()
  local npc = map:get_entity(name)

  npc.on_interaction = function()
    if game:get_value("torin_forest_torina_stone_read") == true then
      game:start_dialog(map_name .. "." .. name .. ".normal")
    else
      game:start_dialog(map_name .. "." .. name .. ".untranslated")
    end
  end
end

return npc