require("scripts/multi_events")

local map_meta = sol.main.get_metatable("map")

map_meta:register_event("on_draw", function(map)

  for entity in map:get_entities() do
    local width, height = entity:get_size()
    local x, y = entity:get_position()
    local origin_x, origin_y = entity:get_origin()
    local sensor_drawable = sol.surface.create(width, height)
    sensor_drawable:fill_color({ 0, 255, 0 })
    sensor_drawable:set_opacity(128)
    map:draw_visual(sensor_drawable, x - origin_x, y - origin_y)
  end
end)
