-- Title screen of the game.

local title_screen = {}

-----------------------------------------------
-- All inits
-----------------------------------------------
function title_screen:on_started()

  title_screen.logo_displayed = false

  -- black screen during 0.3 seconds
  self.phase = "black"

  -- main surface
  self.surface = sol.surface.create(320, 240)
  sol.timer.start(self, 300, function()
    self:phase_title()
  end)

  -- use these 0.3 seconds to preload all sound effects
  sol.audio.preload_sounds()
end

-----------------------------------------------
-- Title phase init
-----------------------------------------------
function title_screen:phase_title()

  -- actual title screen
  self.phase = "title"

  -- start music
  sol.audio.play_music("dungeon_finished")

  -- game logo
  self:init_logo()

  -- clouds
  self:init_clouds()

  -- press space text
  self.press_space_img = sol.text_surface.create{
    font = "kubasta",
    font_size = 10,
    text_key = "title_screen.press_space",
    horizontal_alignment = "center",
    color = { 255, 200, 125 },
  }
  self.press_space_shadow_img = sol.text_surface.create{
    font = "kubasta",
    font_size = 10,
    text_key = "title_screen.press_space",
    horizontal_alignment = "center",
    color = { 190, 53, 33 },
  }
  self.press_space_img:set_opacity(0)
  self.press_space_shadow_img:set_opacity(0)

  self.team_img = sol.text_surface.create({
    font = "kubasta",
    font_size = 10,
    text_key = "title_screen.team",
    horizontal_alignment = "center",
    color = { 80, 95, 160 },
  })
  self.team_img:set_opacity(0)

  -- show an opening transition
  self.surface:fade_in(30)

  -- start game logo
  sol.timer.start(self, 2000, function()
    if self.logo_displayed == false then
      self:start_logo()
    end
  end)

  -- show press start and team
  self.press_start_timer = sol.timer.start(self, 6000, function()
    self.press_space_img:fade_in(60)
    self.press_space_shadow_img:fade_in(60)
    self.team_img:fade_in(60)
  end)

  self.allow_skip = true
end

function title_screen:init_clouds()

  self:init_big_clouds()
  self:init_small_clouds()
  self:init_tiny_clouds()
end

function title_screen:init_big_clouds()

  self.big_cloud_imgs = {
    sol.surface.create("menus/title/big_cloud.png"),
    sol.surface.create("menus/title/big_cloud.png"),
    sol.surface.create("menus/title/big_cloud.png"),
    sol.surface.create("menus/title/big_cloud.png"),
    sol.surface.create("menus/title/big_cloud.png"),
  }
  self.big_cloud_coords = {
    { x = 4, y = 24},
    { x = 144, y = 64},
    { x = 240, y = 188},
    { x = 72, y = 212},
    { x = 284, y = -32},
  }

  sol.timer.start(50, function()
    for cloudIndex = 1, #self.big_cloud_imgs do
      if self.big_cloud_coords[cloudIndex]["y"] > 240 then
        self.big_cloud_coords[cloudIndex]["y"] = -90
      end
      self.big_cloud_coords[cloudIndex]["y"] = self.big_cloud_coords[cloudIndex]["y"] + 1
    end
    return true
  end)
end

function title_screen:init_small_clouds()
  self.small_cloud_imgs = {
    sol.surface.create("menus/title/small_cloud.png"),
    sol.surface.create("menus/title/small_cloud.png"),
    sol.surface.create("menus/title/small_cloud.png"),
    sol.surface.create("menus/title/small_cloud.png"),
  }
  self.small_cloud_coords = {
    { x = 50, y = 0},
    { x = 286, y = 140},
    { x = 112, y = 148},
    { x = 16, y = 196},
  }

  sol.timer.start(75, function()
    for cloudIndex = 1, #self.small_cloud_imgs do
      if self.small_cloud_coords[cloudIndex]["y"] > 240 then
        self.small_cloud_coords[cloudIndex]["y"] = -32
      end
      self.small_cloud_coords[cloudIndex]["y"] = self.small_cloud_coords[cloudIndex]["y"] + 1
    end
    return true
  end)
end

function title_screen:init_tiny_clouds()
  self.tiny_cloud_imgs = {
    sol.surface.create("menus/title/island_1.png"),
    sol.surface.create("menus/title/island_1.png"),
    sol.surface.create("menus/title/island_1.png"),

    sol.surface.create("menus/title/island_2.png"),
    sol.surface.create("menus/title/island_2.png"),
    sol.surface.create("menus/title/island_2.png"),

    sol.surface.create("menus/title/tiny_cloud_1.png"),
    sol.surface.create("menus/title/tiny_cloud_1.png"),
    sol.surface.create("menus/title/tiny_cloud_1.png"),
    sol.surface.create("menus/title/tiny_cloud_1.png"),

    sol.surface.create("menus/title/tiny_cloud_2.png"),
    sol.surface.create("menus/title/tiny_cloud_2.png"),
    sol.surface.create("menus/title/tiny_cloud_2.png"),

    sol.surface.create("menus/title/tiny_cloud_3.png"),
    sol.surface.create("menus/title/tiny_cloud_3.png"),
    sol.surface.create("menus/title/tiny_cloud_3.png"),
    sol.surface.create("menus/title/tiny_cloud_3.png"),
    sol.surface.create("menus/title/tiny_cloud_3.png"),
    sol.surface.create("menus/title/tiny_cloud_3.png"),
    sol.surface.create("menus/title/tiny_cloud_3.png"),
    sol.surface.create("menus/title/tiny_cloud_3.png"),

    sol.surface.create("menus/title/tiny_cloud_4.png"),
    sol.surface.create("menus/title/tiny_cloud_4.png"),
    sol.surface.create("menus/title/tiny_cloud_4.png"),
    sol.surface.create("menus/title/tiny_cloud_4.png"),
    sol.surface.create("menus/title/tiny_cloud_4.png"),
    sol.surface.create("menus/title/tiny_cloud_4.png"),
    sol.surface.create("menus/title/tiny_cloud_4.png"),
    sol.surface.create("menus/title/tiny_cloud_4.png"),
  }
  self.tiny_cloud_coords = {
    { x = 12, y = 12},
    { x = 202, y = 148},
    { x = 96, y = 160},

    { x = 118, y = -4},
    { x = 240, y = 20},
    { x = 30, y = 200},

    { x = 152, y = 34},
    { x = 152, y = 152},
    { x = 240, y = 96},
    { x = 260, y = 200},

    { x = 200, y = 44},
    { x = 288, y = 100},
    { x = 202, y = 160},

    { x = 142, y = 24},
    { x = 224, y = 28},
    { x = 312, y = 86},
    { x = 272, y = 118},
    { x = 260, y = 130},
    { x = 210, y = 208},
    { x = 124, y = 194},
    { x = 50, y = 228},

    { x = 44, y = 36},
    { x = 192, y = 28},
    { x = 280, y = 86},
    { x = 288, y = 136},
    { x = 272, y = 150},
    { x = 210, y = 202},
    { x = 186, y = 210},
    { x = 56, y = 130},
  }

  sol.timer.start(250, function()
    for cloudIndex = 1, #self.tiny_cloud_imgs do
      if self.tiny_cloud_coords[cloudIndex]["y"] > 240 then
        self.tiny_cloud_coords[cloudIndex]["y"] = -50
      end
      self.tiny_cloud_coords[cloudIndex]["y"] = self.tiny_cloud_coords[cloudIndex]["y"] + 1
    end
    return true
  end)
end

function title_screen:init_logo()

  self.mercuris = sol.sprite.create("menus/title/mercuris")
  self.mercuris:set_opacity(0)

  self.sparks = sol.sprite.create("menus/title/sparks")
  self.sparks:set_opacity(0)
  
  self.zelda = sol.sprite.create("menus/title/zelda")
  self.zelda:set_opacity(0)
  
  self.parchment = sol.sprite.create("menus/title/parchment")
  self.parchment:set_opacity(0)

  self.demo = sol.surface.create("menus/title/demo.png")
  self.demo:set_opacity(0)
end

function title_screen:start_logo()
  -- Start mercuris
  self.mercuris:set_animation("stopped")
  self.mercuris:set_opacity(255)

  -- Sparks
  sol.timer.start(self, 500, function()
    self.sparks:set_animation("stopped")
    self.sparks:set_opacity(255)
  end)

  -- Start Zelda
  self.zelda_timer = sol.timer.start(self, 2000, function()
    sol.audio.play_sound("ok")
    self.zelda:set_animation("stopped")
    self.zelda:set_opacity(255)
  end)

  -- Start Parchment
  self.parchment_timer = sol.timer.start(self, 3000, function()
    self.parchment:set_animation("stopped")
    self.parchment:set_opacity(255)
  end)

  -- Set the state of logo appearance
  sol.timer.start(self, 3500, function()
    self.demo:set_opacity(255)
    self.logo_displayed = true
  end)

  -- Shine Zelda periodically
  sol.timer.start(self, 6000, function()
    self.zelda:set_animation("shine")
    return true
  end)
end

function title_screen:force_logo_appearance()

  sol.audio.play_sound("piece_of_heart")

  self.mercuris:set_animation("static")
  self.mercuris:set_opacity(255)

  self.zelda:set_animation("shine")
  self.zelda:set_opacity(255)

  self.parchment:set_animation("static")
  self.parchment:set_opacity(255)
  
  self.demo:set_opacity(255)

  self.press_space_img:set_opacity(255)
  self.press_space_shadow_img:set_opacity(255)
  self.team_img:set_opacity(255)

  if self.zelda_timer ~= nil then
    self.zelda_timer:stop()
  end
  if self.parchment_timer ~= nil then
    self.parchment_timer:stop()
  end
  if self.press_start_timer ~= nil then
    self.press_start_timer:stop()
  end

  -- Make the player wait after force displayed the logo
  sol.timer.start(self, 1000, function()
    self.can_finish_after_forced_display = true
  end)

  self.force_displayed = true
  self.logo_displayed = true
end

-----------------------------------------------
-- All draws
-----------------------------------------------
function title_screen:on_draw(dst_surface)

  if self.phase == "title" then
    self:draw_phase_title()
  end

  -- final blit (dst_surface may be larger)
  local width, height = dst_surface:get_size()
  self.surface:draw(dst_surface, width / 2 - 160, height / 2 - 120)
end

-----------------------------------------------
-- Title phase draw
-----------------------------------------------
function title_screen:draw_phase_title()

  -- background
  self.surface:fill_color({ 90, 140, 239 })

  -- clouds
  self:draw_clouds()

  -- logo
  self:draw_logo()

  -- press space
  self.press_space_shadow_img:draw(self.surface, 161, 175)
  self.press_space_img:draw(self.surface, 160, 174)

  -- team
  self.team_img:draw(self.surface, 160, 210)
end

function title_screen:draw_clouds()

  self:draw_tiny_clouds()
  self:draw_small_clouds()
  self:draw_big_clouds()
end

function title_screen:draw_big_clouds()

  for cloudIndex = 1, #self.big_cloud_imgs do
    self.big_cloud_imgs[cloudIndex]:draw(
      self.surface,
      self.big_cloud_coords[cloudIndex]["x"],
      self.big_cloud_coords[cloudIndex]["y"]
    )
  end
end

function title_screen:draw_small_clouds()

  for cloudIndex = 1, #self.small_cloud_imgs do
    self.small_cloud_imgs[cloudIndex]:draw(
      self.surface,
      self.small_cloud_coords[cloudIndex]["x"],
      self.small_cloud_coords[cloudIndex]["y"]
    )
  end
end

function title_screen:draw_tiny_clouds()

  for cloudIndex = 1, #self.tiny_cloud_imgs do
    self.tiny_cloud_imgs[cloudIndex]:draw(
      self.surface,
      self.tiny_cloud_coords[cloudIndex]["x"],
      self.tiny_cloud_coords[cloudIndex]["y"]
    )
  end
end

function title_screen:draw_logo()
  local origin = { 32, 48 }

  self.mercuris:draw(self.surface, origin[1], origin[2])
  self.sparks:draw(self.surface, origin[1], origin[2])
  self.zelda:draw(self.surface, origin[1] + 47, origin[2] + 9)
  self.parchment:draw(self.surface, origin[1] + 67, origin[2] + 67)
  self.demo:draw(self.surface, origin[1] + 188, origin[2] + 84)
end

-----------------------------------------------
-- Keyboard events
-----------------------------------------------
function title_screen:on_key_pressed(key)

  if key == "escape" then
    -- stop the program
    sol.main.exit()
  elseif key == "space" or key == "return" then
    self:try_finish_title()
  end
end

-----------------------------------------------
-- Joypad events
-----------------------------------------------
function title_screen:on_joypad_button_pressed(button)

  self:try_finish_title()
end

-----------------------------------------------
-- 
-----------------------------------------------
function title_screen:try_finish_title()

  if self.phase == "title" and not self.is_finishing then
    if not self.logo_displayed then
      title_screen:force_logo_appearance()
    else
      if self.force_displayed then
        if self.can_finish_after_forced_display then
          self:finish_title()
        end
      else
        self:finish_title()
      end
    end
  end
end

-----------------------------------------------
-- 
-----------------------------------------------
function title_screen:finish_title()

  self.is_finishing = true
  sol.audio.play_sound("piece_of_heart")
  self.surface:fade_out(30)
  sol.timer.start(self, 700, function()
    sol.audio.stop_music()
    sol.menu.stop(self)
  end)
end

return title_screen

