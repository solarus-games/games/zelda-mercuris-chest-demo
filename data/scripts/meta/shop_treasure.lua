require("scripts/multi_events")

local shop_treasure_meta = sol.main.get_metatable("shop_treasure")

shop_treasure_meta:register_event("on_buying", function(shop_treasure)

  if shop_treasure:get_property("need_empty_bottle") == "true" then
    local game = shop_treasure:get_map():get_game()
    local first_empty_bottle = game:get_first_empty_bottle()
    if first_empty_bottle == nil then
      sol.audio.play_sound("wrong")
      game:start_dialog("_empty_bottle_required")
      return false
    end
  end

  return true
end)