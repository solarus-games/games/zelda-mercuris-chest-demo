
local language_manager = require("scripts/language_manager")

local soon_builder = {}

function soon_builder:new(game, config)
  
  local soon = {}
  soon.visible = false
  soon.top_bar = nil
  soon.bottom_bar = nil
  soon.text = nil

  function soon:on_map_changed(map)
    -- Top black bar
    soon.top_bar = sol.surface.create(320, 32)
    soon.top_bar:fill_color({ 0, 0, 0 })

    -- Bottom back bar
    soon.bottom_bar = sol.surface.create(320, 32)
    soon.bottom_bar:fill_color({ 0, 0, 0 })

    -- Soon text
    local font_name, font_size = language_manager:get_dialog_font()
    soon.text = sol.text_surface.create({
      font = font_name,
      font_size = font_size,
      text = sol.language.get_string("demo_ending.soon")
    })
  end

  function soon:set_visible(visible)
    soon.visible = visible
  end

  function soon:set_text(text_id)
    if text_id == "" then
      soon.text:set_text()
    else
      soon.text:set_text(sol.language.get_string(text_id))
    end
  end

  function soon:on_draw(surface)
    if soon.visible then
      soon.top_bar:draw(surface, config.x, config.y)
      soon.bottom_bar:draw(surface, config.x, config.y + 240 - 32)
      soon.text:draw(surface, config.x + 16, config.y + 240 - 16)
    end
  end

  return soon
end

return soon_builder
