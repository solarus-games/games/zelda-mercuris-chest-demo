local text_builder = {}

function text_builder:create_text(config)

  local text = {}
  text.text_surface = sol.text_surface.create({
    font = config.font or "default",
    font_size = config.font_size or 11,
    text = config.text or "",
    horizontal_alignment = config.horizontal_alignment or "left",
    color = config.color or { 255, 255, 255 }
  })
  text.shadow_color = config.shadow_color or { 0, 0, 0 }
  text.shadow_offset = config.shadow_offset or { 1, 1 }
  text.stroke_color = config.stroke_color or { 0, 0, 0 }
  text.stroke_width = config.stroke_width or 1

  if text.shadow_color then
    text.shadow_text = sol.text_surface.create({
      font = config.font or "default",
      font_size = config.font_size or 11,
      text = config.text or "",
      horizontal_alignment = config.horizontal_alignment or "left",
      color = config.shadow_color
    })
  end

  if text.stroke_color then
    text.stroke_text = sol.text_surface.create({
      font = config.font or "default",
      font_size = config.font_size or 11,
      text = config.text or "",
      horizontal_alignment = config.horizontal_alignment or "left",
      color = config.stroke_color
    })
  end

  function text:draw(dst_surface, x, y)

    -- Draw stroke if any
    if text.stroke_color then
      -- Draw stroked shadow if any
      if text.shadow_color then
        text.shadow_text:draw(dst_surface, x + text.shadow_offset[1] - 1, y + text.shadow_offset[2])
        text.shadow_text:draw(dst_surface, x + text.shadow_offset[1] + 1, y + text.shadow_offset[2])
        text.shadow_text:draw(dst_surface, x + text.shadow_offset[1], y + text.shadow_offset[2] - 1)
        text.shadow_text:draw(dst_surface, x + text.shadow_offset[1], y + text.shadow_offset[2] + 1)
      end

      text.stroke_text:draw(dst_surface, x - 1, y)
      text.stroke_text:draw(dst_surface, x + 1, y)
      text.stroke_text:draw(dst_surface, x, y - 1)
      text.stroke_text:draw(dst_surface, x, y + 1)
    else
      -- Draw simple shadow if any
      if text.shadow_color then
        text.shadow_text:draw(dst_surface, x + text.shadow_offset[1], y + text.shadow_offset[2])
      end
    end

    -- Draw text
    text.text_surface:draw(dst_surface, x, y)
  end

  return text
end

return text_builder