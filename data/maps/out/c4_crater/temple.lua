local map = ...
local game = map:get_game()
local hero = map:get_hero()

local cutscene = require('scripts/maps/cutscene')

function map:on_started()

  -- First talk is done, tannek is waiting on the side
  if game:get_value("crater_temple_tannek_before") == true then
    tannek:set_position(200, 85)
    tannek:get_sprite():set_direction(2)
  end

  -- Mercuris defeated: tannek waiting facing the door
  if game:get_value("dungeon_1_boss") == true then
    tannek:set_position(160, 117)
    tannek:get_sprite():set_direction(1)
  end

  -- Had all talks
  if game:get_value("crater_temple_tannek_after") == true then
    tannek:set_enabled(false)
    tannek_after_sensor:set_enabled(false)
  end
end

-- Talk cutscene before dungeon 1
tannek_before_sensor:register_event("on_activated", function()
  
  if game:get_value("crater_temple_tannek_before") ~= true then
    map:start_before_dungeon_talk()
  end
end)

-- Talk cutscene after defeating Mercuris
tannek_after_sensor:register_event("on_activated", function()

  if game:get_value("dungeon_1_boss") == true then
    map:start_after_dungeon_talk()
  end
end)

function map:start_before_dungeon_talk()

  local link_mov = {
    type = "target",
    entity = hero,
    properties = {
      speed = 64,
      target = { 160, 109 },
      ignore_obstacles = true
    }
  }

  local tannek_mov = {
    type = "target",
    entity = tannek,
    properties = {
      speed = 96,
      target = { 200, 85 },
      ignore_obstacles = true
    }
  }

  hero:freeze()
  cutscene.builder(game, map, hero)
    .wait(250)
    .dialog("crater_temple.tannek.before.1")
    .wait(500)
    .exec(function()
      hero:get_sprite():set_animation("walking")
    end)
    .movement(link_mov)
    .exec(function()
      hero:get_sprite():set_animation("stopped")
    end)
    .wait(250)
    .dialog("crater_temple.tannek.before.2")
    .wait(250)
    .dialog("crater_temple.tannek.before.3")
    .wait(250)
    .dialog("crater_temple.tannek.before.4")
    .wait(250)
    .dialog("crater_temple.tannek.before.5")
    .movement(tannek_mov)
    .exec(function()
      tannek:get_sprite():set_direction(2)
    end)
    .exec(function()
      hero:unfreeze()
      game:set_value("crater_temple_tannek_before", true)
    end)
    .start()
end

function map:start_after_dungeon_talk()

  hero:freeze()

  cutscene.builder(game, map, hero)
    .set_direction(hero, 3)
    .dialog("crater_temple.tannek.after.1")
    .wait(1000)
    .dialog("crater_temple.tannek.after.2")
    .wait(250)
    .dialog("crater_temple.tannek.after.3")
    .wait(250)
    .dialog("crater_temple.tannek.after.4")
    .wait(250)
    .dialog("crater_temple.tannek.after.5")
    .wait(250)
    .dialog("crater_temple.tannek.after.6")
    .wait(250)
    .dialog("crater_temple.tannek.after.7")
    .exec(function()
      hero:unfreeze()

      leave_mov = sol.movement.create("straight")
      leave_mov:set_angle(3 * math.pi / 2)
      leave_mov:set_speed(200)
      leave_mov:set_ignore_obstacles(true)
      leave_mov:set_max_distance(500)
      leave_mov:start(tannek)

      game:set_value("crater_temple_tannek_after", true)
    end)
    .start()
end