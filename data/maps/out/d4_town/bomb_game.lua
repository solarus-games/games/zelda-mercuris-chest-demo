require("scripts/multi_events")

local map = ...
local game = map:get_game()

local bombs_counter = game:get_item("bombs_counter")
local current_bomb_amount = 0
local rupees_destroyed = 0
local game_state = "stopped"

map:register_event("on_started", function(map, destination)
  current_bomb_amount = bombs_counter:get_amount()
end)

wolly:register_event("on_interaction", function ()
  if game_state == "stopped" then
    game:start_dialog("town.bomb_game.1", function()
      if game:has_item("bombs_counter") then
        game:start_dialog("town.bomb_game.2", function(answer)
          map:trade_game(answer)
        end)
      else
        game:start_dialog("town.bomb_game.no_bomb_bag")
      end
    end)
  elseif game_state == "started" then
    game:start_dialog("town.bomb_game.play_the_game")
  end
end)

function map:trade_game(answer)
  if answer == 1 then
    if game:get_money() >= 20 then
      -- pay
      game:remove_money(20)
      -- gives 99 bombs
      bombs_counter:set_max_amount(99)
      current_bomb_amount = bombs_counter:get_amount()
      game:get_item("bombs_counter"):add_amount(99)

      -- explain rules
      game:start_dialog("town.bomb_game.rules", function()
        map:reset_all_rupees()
        map:start_game()
      end)
    else
      sol.audio.play_sound("wrong")
      game:start_dialog("town.bomb_game.not_enough_money")
    end
  end
end

function map:start_game()

  game_state = "running"
  rupees_destroyed = 0

  for rupee in map:get_entities("rupee") do
    rupee:add_collision_test("sprite", function(rupee, other)
      if other:get_type() == "explosion" then
        if rupee:get_sprite():get_opacity() > 0 then
          rupee:get_sprite():set_opacity(0)
          rupees_destroyed = rupees_destroyed + 1
          sol.audio.play_sound("rupee_counter_end")
          if rupees_destroyed >= 12 then
            map:finish_game()
          end
        end
      end
    end)
  end

  map:start_green_rupees()
end

function map:reset_all_rupees()

  for rupee in map:get_entities("rupee") do
    rupee:stop_movement()
    rupee:get_sprite():set_opacity(255)
  end
  rupee_green_1:set_position(64, -3)
  rupee_green_2:set_position(128, -3)
  rupee_green_3:set_position(192, -3)
  rupee_green_4:set_position(256, -3)

  rupee_blue_1:set_position(64, -19)
  rupee_blue_2:set_position(128, -19)
  rupee_blue_3:set_position(192, -19)
  rupee_blue_4:set_position(256, -19)

  rupee_red_1:set_position(64, -35)
  rupee_red_2:set_position(128, -35)
  rupee_red_3:set_position(192, -35)
  rupee_red_4:set_position(256, -35)
end

function map:start_green_rupees()

  local function move_green_rupee(entity, on_returning, on_finished)
    local mov = sol.movement.create("path")
    mov:set_speed(32)
    mov:set_path{6,6,6,6,6,6,6,6,6,6,6,6}
    mov:set_ignore_obstacles(true)
    mov:start(entity, function()
      sol.timer.start(entity, 1000, function()
        if on_returning ~= nil then
          on_returning()
        end
        local back = sol.movement.create("path")
        back:set_speed(32)
        back:set_path{2,2,2,2,2,2,2,2,2,2,2,2}
        back:set_ignore_obstacles(true)
        back:start(entity, function()
          if on_finished ~= nil then
            on_finished()
          end
        end)
      end)
    end)
  end

  move_green_rupee(rupee_green_1, function()
    move_green_rupee(rupee_green_2, function()
      move_green_rupee(rupee_green_3, function()
        move_green_rupee(rupee_green_4, nil, function ()
          map:start_blue_rupees()
        end)
      end)
    end)
  end)
end

function map:start_blue_rupees()
  
  local function move_blue_rupee(entity, side, on_returning, on_finished)
    local first_path = {6,6,6,6,6,6,6,6,6,6,6,6,6,0,0,0,0}
    local second_path = {0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2}

    if side == "left" then
      first_path = {6,6,6,6,6,6,6,6,6,6,6,6,6,4,4,4,4}
      second_path = {4,4,4,4,2,2,2,2,2,2,2,2,2,2,2,2,2}
    end

    local mov = sol.movement.create("path")
    mov:set_speed(32)
    mov:set_path(first_path)
    mov:set_ignore_obstacles(true)
    mov:start(entity, function()
      sol.timer.start(entity, 2000, function()
        if on_returning ~= nil then
          on_returning()
        end
        local back = sol.movement.create("path")
        back:set_speed(32)
        back:set_path(second_path)
        back:set_ignore_obstacles(true)
        back:start(entity, function()
          if on_finished ~= nil then
            on_finished()
          end
        end)
      end)
    end)
  end

  move_blue_rupee(rupee_blue_1, "right")
  move_blue_rupee(rupee_blue_2, "left", function()
    move_blue_rupee(rupee_blue_3, "right")
    move_blue_rupee(rupee_blue_4, "left", nil, function ()
      map:start_red_rupees()
    end)
  end)
end

function map:start_red_rupees()
  
  local function move_red_rupee(entity, on_returning, on_finished)
    local mov = sol.movement.create("path")
    mov:set_speed(64)
    mov:set_path{6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6}
    mov:set_ignore_obstacles(true)
    mov:start(entity, function()
      sol.timer.start(entity, 1000, function()
        if on_returning ~= nil then
          on_returning()
        end
        local back = sol.movement.create("path")
        back:set_speed(64)
        back:set_path{2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2}
        back:set_ignore_obstacles(true)
        back:start(entity, function()
          if on_finished ~= nil then
            on_finished()
          end
        end)
      end)
    end)
  end

  move_red_rupee(rupee_red_1, function()
    move_red_rupee(rupee_red_2, function()
      move_red_rupee(rupee_red_3, function()
        move_red_rupee(rupee_red_4, nil, function ()
          map:finish_game()
        end)
      end)
    end)
  end)
end

function map:finish_game()

  if game_state == "running" then
    game_state = "finished"

    if rupees_destroyed == 12 then
      -- winner!
      game:start_dialog("town.bomb_game.winner", function()
        if not game:has_item("speed_boots") then
          map:get_hero():start_treasure("speed_boots", nil, nil, function()
            game:start_dialog("town.bomb_game.retry", function(answer)
              map:trade_game(answer)
            end)
          end)
        elseif game:get_value("town_bomb_game_piece_of_heart") ~= true then
          -- Piece of heart if already has boots
          map:get_hero():start_treasure("piece_of_heart", nil, nil, function()
            game:start_dialog("town.bomb_game.retry", function(answer)
              map:trade_game(answer)
            end)
          end)
          game:set_value("town_bomb_game_piece_of_heart", true)
        else
          -- 50 rupees if already has boots and piece of heart
          hero:start_treasure("rupee", 4, nil, function()
            game:start_dialog("town.bomb_game.retry", function(answer)
              map:trade_game(answer)
            end)
          end)
        end
      end)
    else
      -- game is lost
      game:start_dialog("town.bomb_game.lost", function(answer)
        map:trade_game(answer)
      end)
    end

    game_state = "stopped"

    map:reset_bomb_bag()
  end
end

-- Reset the game after loosing
function map:reset_game()
  for rupee in map:get_entities("rupee") do
    rupee:get_sprite():set_opacity(255)
  end
  rupees_destroyed = 0
  map:start_game()
end

function map:reset_bomb_bag()
  bombs_counter:set_amount(current_bomb_amount)
  bombs_counter:set_max_amount(10)
end

-- Restore initial bomb bag when leaving the game
reset_bombs:register_event("on_activated", function()
  map:reset_bomb_bag()
end)
