local audio = require("scripts/util/audio")

local map = ...
local game = map:get_game()
local hero = map:get_hero()

local function jump_from_bed()
  game:set_hud_enabled(true)
  game:set_value("town_inn_awakening", true)
  game:set_pause_allowed(true)

  hero:unfreeze()
  hero:set_visible(true)
  hero:start_jumping(0, 24, true)
  
  bed:get_sprite():set_animation("empty_open")
  sol.audio.play_sound("hero_lands")
end

function map:on_started(destination)

  if game:get_value("town_inn_awakening") ~= true then
    -- From intro cutscene
    audio:reset_music_volume()
    hero:set_visible(false)
    game:set_hud_enabled(false)
    game:set_pause_allowed(false)
    
    local bed_sprite = bed:get_sprite()
    bed_sprite:set_animation("hero_sleeping")
    
    sol.timer.start(map, 5000, function()
      bed_sprite:set_animation("hero_waking")
      snores:remove()
      sol.timer.start(map, 2000, function()
        jump_from_bed()
      end)
    end)
  else
    -- From stairs or save
    snores:remove()
    if destination == awakening then
      -- From save only
      local x, y = hero:get_position()
      hero:set_position(x + 32, y)
    end
  end
end
