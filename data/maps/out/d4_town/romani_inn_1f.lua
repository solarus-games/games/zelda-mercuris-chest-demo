require("scripts/multi_events")

local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  if game:get_value("dungeon_1_boss") == true then
    for entity in map:get_entities("packets") do
      entity:set_enabled(false)
    end
  end
end)

local function anju_talks()
  if game:get_value("town_inn_anju_first_talk") ~= true then
    -- First talk
    game:start_dialog("town.romani_inn.anju.first")
    game:set_value("town_inn_anju_first_talk", true)

  else
    if game:get_value("dungeon_1_boss") ~= true then
      -- Before dungeon 1
      game:start_dialog("town.romani_inn.anju.before_dungeon_1")
    else
      -- After dungeon 1
      game:start_dialog("town.romani_inn.anju.after_dungeon_1")
    end
  end
end

local function bartender_talks()
  game:start_dialog("town.romani_inn.bartender.1", function(answer)
    if answer == 1 then
      if game:get_money() < 25 then
        sol.audio.play_sound("wrong")
        game:start_dialog("_shop.not_enough_money")
      elseif game:get_first_empty_bottle() == nil then
        sol.audio.play_sound("wrong")
        game:start_dialog("town.romani_inn.bartender.empty_bottle_needed")
      else
        game:remove_money(25)
        hero:start_treasure("milk")
      end
    end
  end)
end

anju:register_event("on_interaction", anju_talks)
anju_sensor:register_event("on_interaction", anju_talks)
bartender_sensor:register_event("on_interaction", bartender_talks)