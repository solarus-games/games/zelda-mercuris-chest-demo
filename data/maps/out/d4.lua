require("scripts/multi_events")
local cutscene = require('scripts/maps/cutscene')
local npc = require("scripts/maps/npc")

local map = ...
local game = map:get_game()
local hero = map:get_hero()
local camera = map:get_camera()

map:register_event("on_started", function()

  map:init_tannek_interactions()
  map:init_all_traders()
  npc:init_talk_with_torin("fugo", "town")
end)

function map:on_opening_transition_finished()

  -- Force the hero freeze when the map opening is really finished
  if game:get_value("town_first_visit") ~= true then
    hero:freeze()
  end
end

function map:init_tannek_interactions()

  if game:get_value("town_first_visit") ~= true then
    -- Launch first visit cutscene
    map:start_first_visit_cutscene()
  elseif game:get_value("town_aroma_second_talk") == true
    and game:get_value("town_tannek_torin_talk") ~= true then
    -- Tannek after aroma talk about chest stolen
    map:tannek_talk_after_dungeon_1()
  else
    -- No tannek
    tannek:remove()
  end
end

--------------------------------
-- First visit cutscene
--------------------------------
function map:start_first_visit_cutscene()
  
  game:set_hud_enabled(false)
  game:set_pause_allowed(false)
  camera:start_manual()
  map:visit_housing_quarter()
end

function map:visit_housing_quarter()

  -- Init position
  camera:set_position(1280, 424)

  -- Move Camera
  local mov = sol.movement.create("target")
  mov:set_target(1180, 424)
  mov:set_speed(32)
  mov:start(camera, function()
    mov:stop()
    map:visit_musical_quarter()
  end)
end

function map:visit_musical_quarter()

  -- Init position
  camera:set_position(96, 1060)

  -- Move Camera
  local mov = sol.movement.create("target")
  mov:set_target(96, 946)
  mov:set_speed(32)
  mov:start(camera, function()
    mov:stop()
    map:visit_park()
  end)
end

function map:visit_park()

  -- Init position
  camera:set_position(1316, 1604)

  -- Move camera
  local mov = sol.movement.create("target")
  mov:set_target(1416, 1604)
  mov:set_speed(32)
  mov:start(camera, function()
    mov:stop()
    map:return_from_clock_tower()
  end)
end

function map:return_from_clock_tower()
  
  -- Init position
  camera:set_position(856, 912)

  -- Move camera
  local mov = sol.movement.create("target")
  mov:set_target(856, 720)
  mov:set_speed(64)
  mov:set_ignore_obstacles(true)
  mov:start(camera, function()
    mov:stop()
    map:finish_visit()
  end)
end

function map:finish_visit()

  -- Init position
  local hero_x, hero_y = hero:get_position()
  camera:set_position(hero_x - 160, hero_y + 96)
  map:start_tannek_movement()

  -- Move camera
  local mov = sol.movement.create("target")
  mov:set_target(hero_x - 160, hero_y - 120)
  mov:set_speed(64)
  mov:set_ignore_obstacles(true)
  mov:start(camera, function()
    mov:stop()
    game:set_hud_enabled(true)
    camera:start_tracking(hero)
  end)
end

function map:start_tannek_movement()

  -- Create tannek movement
  local mov = sol.movement.create("target")
  mov:set_speed(24)
  mov:set_target(1152, 333)
  mov:start(tannek, function()
    mov:stop()
    tannek:stop_movement()
    map:start_first_talk_with_tannek()
  end)

  tannek:get_sprite():set_frame_delay(500)
end

--------------------------------
-- First talk with tannek
--------------------------------

function map:start_first_talk_with_tannek()
  
  -- Movement of Tannek going to Link for dialog
  local meet_mov = {
    type = "target",
    entity = tannek,
    properties = {
      speed = 200,
      target = { 1112, 272 },
      ignore_obstacles = true
    }
  }

  local leave_mov = {
    type = "path",
    entity = tannek,
    properties = {
      speed = 200,
      ignore_obstacles = true
    }
  }

  cutscene.builder(game, map, hero)
    .dont_wait_for.sprite_animation(tannek:get_sprite(), "stopped")
    .wait(500)
    .set_direction(tannek:get_sprite(), 1)
    .wait(500)
    .dialog("town.tannek.first.1")
    .movement(meet_mov)
    .set_direction(tannek:get_sprite(), 1)
    .wait(250)
    .dialog("town.tannek.first.2")
    .wait(250)
    .dialog("town.tannek.first.3")
    .wait(250)
    .hero_start_treasure("rupee_bag", 1, "town_rupee_bag")
    .wait(250)
    .dialog("town.tannek.first.4")
    .wait(250)
    .movement(leave_mov, function(mov)
      mov:set_path{ 6, 6, 6, 6, 6, 6, 6, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 }
    end)
    .exec(map.end_first_talk_with_tannek)
    .start()
end

function map:end_first_talk_with_tannek()

  hero:unfreeze()
  game:set_pause_allowed(true)
  tannek:remove()
  game:set_value("town_first_visit", true)
end

--------------------------------
-- Small traders
--------------------------------

function map:init_all_traders()

  npc:setup_seller(seller_a, { product = "heart_one" })
  npc:setup_seller(seller_b, { product = "magic_small" })
  npc:setup_seller(seller_c, { product = "bomb_eight" })
  npc:setup_seller(seller_d, { product = "apple_one" })
  npc:setup_seller(seller_e, { product = "bomb_three" })
  npc:setup_seller(seller_f, { product = "magic_big" })
  npc:setup_seller(seller_g, { product = "fairy" })
  npc:setup_seller(seller_h, { product = "heart_three" })
  npc:setup_seller(seller_i, { product = "croissant_one" })
  npc:setup_seller(seller_j, { product = "bomb_one" })
  npc:setup_seller(seller_k, { product = "pain_au_chocolat_one" })

  npc:setup_buyer(buyer_a, { product = "heart_one" })
  npc:setup_buyer(buyer_e, { product = "bomb_three" })
  npc:setup_buyer(buyer_b, { product = "bomb_one" })
  npc:setup_buyer(buyer_c, { product = "heart_three" })
  npc:setup_buyer(buyer_d, { product = "magic_small" })
  npc:setup_buyer(buyer_f, { product = "bomb_eight" })
  npc:setup_buyer(buyer_g, { product = "magic_big" })
end

--------------------------------
-- Tannek talk after dungeon 1
--------------------------------

function map:tannek_talk_after_dungeon_1()

  hero:freeze()
  game:set_hud_enabled(false)
  tannek:set_position(208, 1797)
  cutscene.builder(game, map, hero)
    .wait(1000)
    .dialog("town.tannek.torin.1")
    .exec(function()
      hero:get_sprite():set_direction(0)
    end)
    .dialog("town.tannek.torin.2")
    .dialog("town.tannek.torin.3")
    .dialog("town.tannek.torin.4")
    .exec(function()
      hero:unfreeze()
      game:set_hud_enabled(true)
      game:set_value("town_tannek_torin_talk", true)
    end)
    .exec(function()
      local mov = sol.movement.create("straight")
      mov:set_angle(0)
      mov:set_speed(200)
      mov:set_ignore_obstacles(true)
      mov:set_max_distance(500)
      mov.on_finished = function()
        tannek:remove()
      end
      mov:start(tannek)
    end)
    .start()
end

--------------------------------
-- Malonia
--------------------------------
malonia:register_event("on_interaction", function()
  
  local dialog_id = "town.malonia."

  if game:get_value("town_malonia_first_talk") ~= true then
    dialog_id = dialog_id .. "first"
  else
    dialog_id = dialog_id .. "second"
  end

  if game:has_ability("sword", 1) then
    dialog_id = dialog_id .. "_normal"
  else
    dialog_id = dialog_id .. "_no_sword"
  end
  
  game:start_dialog(dialog_id)
  game:set_value("town_malonia_first_talk", true)
end)

--------------------------------
-- Balek
--------------------------------

balek:register_event("on_interaction", function()

  game:start_dialog("town.balek.1", game:get_player_name())
end)