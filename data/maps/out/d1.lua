local ending = require("scripts/util/ending")

local map = ...

function map:on_started()

  ending:start("out/f7")
end

function map:on_opening_transition_finished()

  ending:init_hero()
end
