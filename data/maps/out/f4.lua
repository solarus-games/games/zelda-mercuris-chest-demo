local ending = require("scripts/util/ending")

local map = ...

function map:on_started()

  ending:start_static(6000, function()
    map:get_hero():teleport("out/a7")
  end)
end

function map:on_opening_transition_finished()

  ending:init_hero()
end
