local ending = require("scripts/util/ending")

local map = ...

function map:on_started()

  ending:start("out/f3", function()
    sol.audio.play_music("overworld")
  end)
end

function map:on_opening_transition_finished()

  ending:init_hero()
end
