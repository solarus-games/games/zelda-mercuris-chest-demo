require("scripts/multi_events")
local util = require("scripts/util/dungeons")

local map = ...
local game = map:get_game()

-- Miniboss may have been already defeated,
-- check for its value on map
map:register_event("on_started", function(map, destination)

  map:set_doors_open("miniboss_door", true)
  if greenlizalfos == nil then
    map:set_doors_open("blocker_2", true)
    map:set_doors_open("blocker_1", true)
    blocker_1_switch:set_activated(true)
  end
  util:disable_all_enemies_except_for_current_room(map)
  util:init_weak_wall("dungeon_1", 1)
end)

-- Miniboss save state
local miniboss_sensor_activated = game:get_value("dungeon_1_miniboss")

-- Close doors and activate miniboss only if not already defeated
miniboss_sensor:register_event("on_activated", function()
  
  if miniboss_sensor_activated ~= true then
    map:close_doors("blocker_1")
    sol.audio.play_music("shield/oot_mini_boss")
    miniboss_sensor_activated = true
  end
end)

-- Mini boss defeated: open doors and restart dungeon music
if greenlizalfos ~= nil then
  greenlizalfos:register_event("on_dead", function()
    miniboss_sensor_activated = true
    map:open_doors("blocker_1")
    map:open_doors("blocker_2")
    sol.audio.play_music("light_world_dungeon")
    sol.audio.play_sound("secret")
    game:set_value("dungeon_1_miniboss", true)
  end)
end
