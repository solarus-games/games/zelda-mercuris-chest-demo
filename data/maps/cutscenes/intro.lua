local audio = require("scripts/util/audio")

local map = ...
local hero = map:get_hero()
local game = map:get_game()
local camera = map:get_camera()

local _, odhar_initial_y = odhar:get_position()
local _, godhar_initial_y = godhar:get_position()

function map:on_started()

  map:set_joypad_commands()
 
  hero:set_visible(false)
  game:set_hud_enabled(false)

  map:init_camera()
  map:init_seagulls()
  map:init_fishes()
  map:move_camera()

  sol.audio.play_music("intro")
end

function map:on_opening_transition_finished()

  hero:freeze()
  game:set_pause_allowed(false)
end

function map:set_joypad_commands()

  -- Button mapping according commonly used xbox gamepad on PC
  game:set_command_joypad_binding("action", "button 0") -- button 0 = A (xbox/pc)
  game:set_command_joypad_binding("attack", "button 2") -- button 2 = X (xbox/pc)
  game:set_command_joypad_binding("item_1", "button 3") -- button 3 = Y (xbox/pc)
  game:set_command_joypad_binding("item_2", "button 1") -- button 1 = B (xbox/pc)
  game:set_command_joypad_binding("pause", "button 7")  -- button 7 = Menu/Start (xbox/pc)
  game:set_command_joypad_binding("up", "axis 1 -")
  game:set_command_joypad_binding("left", "axis 0 -")
  game:set_command_joypad_binding("right", "axis 0 +")
  game:set_command_joypad_binding("down", "axis 1 +")
end

function map:init_fishes()

  for fish in map:get_entities("fish") do
    local mov = sol.movement.create("straight")
    mov:set_ignore_obstacles(true)
    mov:set_angle(math.pi)
    mov:set_speed(4)
    mov:start(fish)
  end
end

function map:init_seagulls()

  for seagull in map:get_entities("seagull") do
    local mov = sol.movement.create("straight")
    mov:set_ignore_obstacles(true)
    mov:set_angle(math.pi)
    mov:set_speed(8)
    mov:start(seagull)
  end
end

function map:init_camera()
  -- Move the camera to the left
  camera:set_position(camera_initial_spot:get_position())
  camera:start_manual()
end

function map:move_camera()
  local camera_mov = sol.movement.create("straight")
  camera_mov:set_ignore_obstacles(true)
  camera_mov:set_angle(math.pi)
  camera_mov:set_speed(68)
  camera_mov:set_max_distance(972)
  camera_mov:start(camera)

  -- Start to move the boat when the camera is near
  local boat_is_moving = false
  local camera_boat_spot_x, _ = camera_boat_spot:get_position()
  function camera_mov:on_position_changed()
    local x, y = camera_mov:get_xy()
    if boat_is_moving == false and x <= camera_boat_spot_x then
      boat_is_moving = true
      map:move_boat()
    end
  end
end

function map:move_boat()

  -- Create boat movement
  local boat = map:get_entity("boat")
  local boat_mov = sol.movement.create("straight")
  boat_mov:set_ignore_obstacles(true)
  boat_mov:set_angle(math.pi)
  boat_mov:set_speed(28)
  boat_mov:set_max_distance(240)
  boat_mov:start(boat, map.odhar_join_godhar)

  -- Get bad guys
  local odhar = map:get_entity("odhar")
  local godhar = map:get_entity("godhar")

  -- Adjust bad guys position on the boat
  boat_mov.on_position_changed = function()
    local x, y = boat_mov:get_xy()
    odhar:set_position(x + 320, odhar_initial_y)
    godhar:set_position(x + 72, godhar_initial_y)
  end
end

function map:odhar_join_godhar()
  local mov = sol.movement.create("path")
  mov:set_speed(64)
  mov:set_ignore_obstacles(true)
  mov:set_path{3,3,3,3,4,5,5,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,5,6,5,4}
  mov:start(odhar, map.bad_guys_jump)
end

local function jump(entity)

  local sprite = entity:get_sprite()
  entity:create_sprite("entities/shadow")
  
  -- Constants
  local height = 4
  local step = 0.16

  -- States
  local delta = height
  local velocity = 0
  local state = "jumping"

  sol.audio.play_sound("throw")

  sol.timer.start(entity, 16, function()
  
    if delta > -height then
      delta = delta - step
    elseif delta < -height then
      delta = -height
    else
      state = "idle"
    end

    if state ~= "idle" then
      velocity = velocity - delta
    else
      velocity = 0
      sol.audio.play_sound("hero_lands")
    end

    sprite:set_xy(0, velocity)

    return state == "jumping"
  end)
end

function map:bad_guys_jump()
  
  local godhar_jump = sol.movement.create("target")
  godhar_jump:set_speed(160)
  godhar_jump:set_target(568, 349)
  godhar_jump:set_ignore_obstacles(true)
  godhar_jump:start(godhar)
  
  jump(godhar)

  sol.timer.start(map, 250, function()
    local odhar_jump = sol.movement.create("target")
    odhar_jump:set_speed(160)
    odhar_jump:set_target(584, 349)
    odhar_jump:set_ignore_obstacles(true)
    odhar_jump:start(odhar, map.bad_guys_walk_in_house)

    jump(odhar)
  end)
end

function map:bad_guys_walk_in_house()

  godhar:set_layer(1)
  odhar:set_layer(1)

  local godhar_walk = sol.movement.create("path")
  godhar_walk:set_speed(64)
  godhar_walk:set_path{4,3,3,2,2,2,2,2,2,2,2,2,2,2}
  godhar_walk:set_ignore_obstacles(true)
  godhar_walk:start(godhar, function()
    godhar:remove()
  end)

  sol.timer.start(map, 250, function()
    local odhar_walk = sol.movement.create("path")
    odhar_walk:set_speed(64)
    odhar_walk:set_path{4,4,4,3,3,2,2,2,2,2,2,2,2,2,2,2}
    odhar_walk:set_ignore_obstacles(true)
    odhar_walk:start(odhar, function()
      odhar:remove()
      sol.timer.start(map, 1000, function()
        map:finish()
      end)
    end)
  end)
end

function map:finish()
  map:get_hero():teleport("cutscenes/intro_house", "from_intro")
end

function map:on_draw(dst_surface)
  -- Add black bars for the cutscene
  local black = { 0, 0, 0 }
  dst_surface:fill_color(black, 0, 0, 320, 24)
  dst_surface:fill_color(black, 0, 240 - 24, 320, 24)
end
