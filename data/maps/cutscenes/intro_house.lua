local map = ...
local game = map:get_game()

local cutscene = require("scripts/maps/cutscene")
local audio = require("scripts/util/audio")
local language_manager = require("scripts/language_manager")

local black_fade
local one_day_after_text

function map:on_started(destination)

  black_fade = sol.surface.create()
  black_fade:fill_color({ 0, 0, 0 })
  black_fade:set_opacity(0)

  local font, font_size = language_manager:get_dialog_font()
  one_day_after_text = sol.text_surface.create({
    horizontal_alignment = "center",
    text = sol.language.get_string("intro.one_day_after"),
    font = font,
    font_size = font_size,
    color = {255,255,255},
  })
  one_day_after_text:set_opacity(0)

  if destination == from_intro then
    -- Intro cutscene.
    game:set_hud_enabled(false)
    game:set_pause_allowed(false)
    snores:get_sprite():set_ignore_suspend(true)
    bed:get_sprite():set_animation("hero_sleeping")
    hero:set_visible(false)

    -- Change music channels
    sol.audio.set_music_channel_volume(0, 0) -- melody left muted
    sol.audio.set_music_channel_volume(2, 0) -- snare left muted
    sol.audio.set_music_channel_volume(4, 0) -- trumpet 2 left muted
    sol.audio.set_music_channel_volume(8, 0) -- melody right muted
    sol.audio.set_music_channel_volume(10, 0) -- snare right muted
    sol.audio.set_music_channel_volume(12, 0) -- trumpet 2 left muted
  end
end

local function play_cutscene()

  local camera = map:get_camera()
  local _, map_height = map:get_size()
  local _, camera_height = camera:get_size()
  local bad_guys_speed = 64
  hero:freeze()
  cutscene.builder(game, map, hero)
    .wait(2000)
    .movement({
      entity = camera,
      type = "straight",
      properties = {
        speed = 96,
        angle = 3 * math.pi / 2,
        max_distance = map_height - camera_height,
        ignore_obstacles = true,
      },
    })
    .exec(function()
      camera:start_tracking(godhar)
      sol.timer.start(map, 1000, function()
        godhar:set_layer(godhar:get_layer() + 1)
        odhar:set_layer(odhar:get_layer() + 1)
      end)
    end)
    .dont_wait_for
    .movement({
      entity = godhar,
      type = "path",
      properties = {
        speed = bad_guys_speed,
        path = {{ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                  4, 4, 4, 4,
                  2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               }},
        ignore_obstacles = true,
      },
    })
    .wait(500)
    .movement({
      entity = odhar,
      type = "path",
      properties = {
        speed = bad_guys_speed,
        path = {{ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                  4, 4, 4, 4,
                  2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               }},
        ignore_obstacles = true,
      },
    })
    .wait(1000)
    .set_direction(odhar:get_sprite(), 3)
    .wait(500)
    .dialog("intro.bad_guys.1")
    .wait(500)
    .set_direction(godhar:get_sprite(), 1)
    .wait(500)
    .dialog("intro.bad_guys.2")
    .wait(500)
    .set_direction(odhar:get_sprite(), 0)
    .set_direction(godhar:get_sprite(), 0)
    .wait(1000)
    .dialog("intro.bad_guys.3", game:get_player_name())
    .wait(500)
    .exec(function()
      bed:get_sprite():set_animation("empty_open")
      snores:remove()
      hero:set_animation("stopped")
      hero:set_direction(2)
      hero:set_visible(true)
    end)
    .wait(500)
    .music("soldiers")
    .enable(hero_question_mark)
    .wait(200)
    .enable(godhar_exclam)
    .enable(odhar_exclam)
    .dont_wait_for
    .hero_animation("hurt")
    .wait(1000)
    .exec(function()
      odhar_bag:set_enabled(true)
      odhar:set_enabled(false)
      local sprite = odhar_bag:get_sprite()
      sprite:set_animation("catch")
      sprite.on_animation_finished = function()
        sprite:set_animation("stopped")
      end
    end)
    .sound("hero_hurt")
    .set_direction(hero, 0)
    .dont_wait_for
    .hero_animation("dying")
    .exec(function()
      hero:set_enabled(false)
    end)
    .wait(300)
    .exec(function()
      camera:start_manual()
    end)
    .wait(1000)
    .dont_wait_for
    .movement({
      entity = odhar_bag,
      type = "path",
      properties = {
        speed = bad_guys_speed,
        path = {{ 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
                  6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
               }},
        ignore_obstacles = true,
      },
    })
    .movement({
      entity = godhar,
      type = "path",
      properties = {
        speed = bad_guys_speed,
        path = {{ 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
                  6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
               }},
        ignore_obstacles = true,
      },
    })
    .wait(1000)
    .exec(function()
      audio:music_fade_out(2000)
      black_fade:fade_in(25, function()
        sol.timer.start(map, 1000, function()
          one_day_after_text:fade_in(25)
          sol.timer.start(map, 4000, function()
            hero:set_enabled(true)
            hero:teleport("out/d4_town/romani_inn_2f", "awakening")
          end)
        end)
      end)
    end)
    .start()
end

function map:on_opening_transition_finished(destination)

  if destination == from_intro then
    play_cutscene()
  end
end

function map:on_draw(dst_surface)
  -- Add black bars for the cutscene
  local black = { 0, 0, 0 }
  dst_surface:fill_color(black, 0, 0, 320, 24)
  dst_surface:fill_color(black, 0, 240 - 24, 320, 24)

  black_fade:draw(dst_surface)
  one_day_after_text:draw(dst_surface, 160, 120)
end
