local enemy = ...

local behavior = require("enemies/lib/towards_hero")

local properties = {
  sprite = "enemies/" .. enemy:get_breed(),
  life = 16,
  damage = 2,
  normal_speed = 40,
  faster_speed = 48,
  pushed_when_hurt = false
}

behavior:create(enemy, properties)