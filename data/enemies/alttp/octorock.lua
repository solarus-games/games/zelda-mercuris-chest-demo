local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

local state = "run"

function enemy:on_created()

  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(2)
  enemy:set_damage(2)
  enemy:set_size(16, 16)
  enemy:set_origin(8, 13)
end

function enemy:on_restarted()

  sol.timer.start(enemy, math.random(6, 12) * 1000, function()
    state = "shoot"
    enemy:shoot()
  end)

  if state == "shoot" then
    enemy:shoot()
  else
    enemy:run()
  end
end

local function get_opposite_direction(direction)

  local opposites = { 2, 3, 0, 1 }
  return opposites[direction]
end

local function get_opposite_angle(direction)

  local opposites = {
    math.pi,
    3 * math.pi / 2,
    0,
    math.pi / 2,
  }

  return opposites[direction]
end

function enemy:run()

  local angles = {
    0,
    math.pi / 2,
    math.pi,
    3 * math.pi / 2,
  }

  local random_direction = math.random(0, 3)
  local random_angle = angles[random_direction + 1]

  sprite:set_animation("walking")
  sprite:set_direction(random_direction)

  movement = sol.movement.create("straight")
  movement:set_angle(random_angle)
  movement:set_speed(96)
  movement:set_smooth(false)
  movement:start(enemy)
  movement.on_obstacle_reached = function()
    local opposite_direction = get_opposite_direction(sprite:get_direction() + 1)
    local opposite_angle = get_opposite_angle(sprite:get_direction() + 1)
    sprite:set_direction(opposite_direction)
    movement:set_angle(opposite_angle)
  end
end

function enemy:shoot()

  local shoot_number = math.random(8, 16)
  local current_direction = sprite:get_direction()

  movement:stop()

  sprite:set_animation("inhale")

  sol.timer.start(enemy, 500, function()
    sprite:set_animation("shooting")

    sol.timer.start(enemy, 250, function()

      local x, y, layer = enemy:get_position()
      
      -- Play sound if hero is near enough
      local hero_x, hero_y = hero:get_position()
      if math.abs(x - hero_x) < 184 and math.abs(y - hero_y) < 144 then
        sol.audio.play_sound("octorock")
      end
      
      -- Create the rock enemy entity
      map:create_enemy({
        breed = "alttp/octorock_rock",
        layer = layer,
        x = x,
        y = y,
        direction = current_direction,
      })
  
      shoot_number = shoot_number - 1
      current_direction = current_direction + 1
      if current_direction >= 4 then
        current_direction = 0
      end
  
      sprite:set_direction(current_direction)
  
      if shoot_number <= 0 then
        state = "run"
        enemy:on_restarted()
      end
  
      return shoot_number > 0
    end)
  end)
end
