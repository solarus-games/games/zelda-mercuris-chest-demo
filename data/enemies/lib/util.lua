local util = {}

function util.enrich(enemy)

  function enemy:is_near(entity, distance)
    
    local entity_layer = entity:get_layer()
    local x, y, layer = enemy:get_position()

    return entity:get_distance(x, y) < distance
      and (layer == entity_layer or enemy:has_layer_independent_collisions())
  end
end

return util