return function(enemy)

  local properties = {}
  local main_sprite = nil

  local map = enemy:get_map()
  local hero = map:get_hero()
  local movement

  function enemy:set_properties(prop)

    properties = prop
  end

  function enemy:on_created()

    main_sprite = enemy:create_sprite(properties.main_sprite)

    enemy:set_life(1)
    enemy:set_damage(2)
    --enemy:set_size(8, 8)
    --enemy:set_origin(8, 13)
    enemy:set_layer_independent_collisions(true)
    --enemy:set_attacking_collision_mode("overlapping")
  end

  function enemy:on_restarted()
    
    movement = sol.movement.create("straight")
    movement:set_angle(math.pi / 2) -- up
    movement:set_speed(16)
    movement:set_ignore_obstacles(true)
    movement:set_max_distance(16)
    movement:start(enemy)
    movement.on_finished = function()
      movement:stop()
      enemy:rush_to_hero()
    end
  end

  function enemy:rush_to_hero()

    sol.audio.play_sound("alttp/keese")
    local hero_x, hero_y = hero:get_position()
    local angle = enemy:get_angle(hero_x, hero_y)
    movement = sol.movement.create("straight")
    movement:set_speed(128)
    movement:set_angle(angle)
    movement:set_ignore_obstacles(true)
    movement:set_smooth(false)
    movement:set_max_distance(400)
    movement:start(enemy)
    movement.on_finished = function()
      enemy:remove()
    end
  end
end
