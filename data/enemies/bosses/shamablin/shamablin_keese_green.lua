local enemy = ...

require("enemies/bosses/shamablin/lib/shamablin_keese")(enemy)
enemy:set_properties({
  main_sprite = "enemies/bosses/shamablin/shamablin_keese_green",
})
