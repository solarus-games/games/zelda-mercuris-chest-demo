local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

local state = "circle"
local target_x = 0
local target_y = 0
local on_placed_callback

function enemy:on_created()

  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(1)
  enemy:set_damage(1)
  enemy:set_invincible(true)
  
end

local function get_sprite_direction_from_angle(angle)
  if angle >= 15 * math.pi / 8 and angle < 1 * math.pi / 8 then
    return 3
  elseif angle >= 1 * math.pi / 8 and angle < 3 * math.pi / 8 then
    return 4
  elseif angle >= 3 * math.pi / 8 and angle < 5 * math.pi / 8 then
    return 5
  elseif angle >= 5 * math.pi / 8 and angle < 7 * math.pi / 8 then
    return 6
  elseif angle >= 7 * math.pi / 8 and angle < 9 * math.pi / 8 then
    return 7
  elseif angle >= 9 * math.pi / 8 and angle < 11 * math.pi / 8 then
    return 0
  elseif angle >= 11 * math.pi / 8 and angle < 13 * math.pi / 8 then
    return 1
  else
    return 2
  end
end

function enemy:on_restarted()

  if state == "circle" then
    local x, y = enemy:get_position()
    movement = sol.movement.create("circle")
    movement:set_center(x, y)
    movement:set_radius(24)
    movement:set_angular_speed(1.5 * math.pi)
    movement:set_ignore_obstacles(true)
    movement:start(enemy)
    movement.on_position_changed = function()
      local angle = movement:get_angle_from_center()
      local direction = get_sprite_direction_from_angle(angle)
      sprite:set_direction(direction)
    end
  elseif state == "placing" then
    movement = sol.movement.create("target")
    movement:set_target(target_x, target_y)
    movement:set_speed(192)
    movement:set_ignore_obstacles(true)
    movement:start(enemy)
    movement.on_finished = function()
      movement:stop()
      sprite:set_direction(6)
      on_placed_callback()
    end
  elseif state == "dashing" then
    movement = sol.movement.create("straight")
    movement:set_angle(3 * math.pi / 2)
    movement:set_speed(96)
    movement:start(enemy)
    movement.on_obstacle_reached = function()
      enemy:remove()
    end
  end
end

function enemy:place(x, y, on_placed)

  state = "placing"
  target_x = x
  target_y = y
  on_placed_callback = on_placed

  enemy:on_restarted()
end

function enemy:dash()

  state = "dashing"
  enemy:on_restarted()
end
