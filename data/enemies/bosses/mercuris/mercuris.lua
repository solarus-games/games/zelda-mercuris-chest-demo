local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement
local shadow
local shield

local state = "falling_intro"

local back_walk_y = 0
local jump_height = 5
local jump_delta = 0
local jump_velocity = 0

-- Initialization
function enemy:on_created()

  shadow = enemy:create_sprite("enemies/bosses/mercuris/mercuris_shadow")
  shield = enemy:create_sprite("enemies/bosses/mercuris/mercuris_shield")
  sprite = enemy:create_sprite("enemies/bosses/mercuris/mercuris")
  shadow:set_animation("stopped")
  enemy:set_life(7)
  enemy:set_damage(2)
  enemy:set_pushed_back_when_hurt(false)
  enemy:set_hurt_style("boss")
  enemy:set_size(32, 32)
  enemy:set_attacking_collision_mode("origin")
end

-- Called on init and after hurt
function enemy:on_restarted()

  -- Intro sequence state
  if state == "falling_intro" then
    enemy:fall_from_ceiling(function()
      -- Fall sequence finished
      sol.audio.play_music("boss")
      hero:unfreeze()
      enemy:do_first_attack()
    end)

  -- Guard state (after being hurt)
  elseif state == "guard" then
    if enemy:get_life() > 1 then
      enemy:guard()
      enemy:do_random_next_attack()
    else
      -- Boss has no more life
      enemy:set_shield_enabled(true)
      enemy:set_animation("hurt")
      enemy:leave_with_chest()
    end
  end

  -- Set Mercuris vulnerable after 10 seconds
  sol.timer.start(enemy, 10000, function()
    enemy:set_shield_enabled(false)
  end)
end

-- Called on every frame
function enemy:on_update()

  -- Match the X coord to Link's X coord for Mercuris to aim during
  -- his punch or dash attack
  if state == "positionning_dash" then
    local hero_x, _ = hero:get_position()
    enemy:set_position(hero_x, back_walk_y)
  end
end

-- Called on hurt
function enemy:on_hurt()

  -- Because the engine is calling on_restarted after being hurt,
  -- We set the state to guard so Mercuris can be hurt only once and then
  -- guards and perform a next attack
  state = "guard"

  -- Prevent Mercuris from dying if the player managed to put his life to 0
  if enemy:get_life() <= 0 then
    enemy:set_life(1)
  end

  -- Remove all possibly existing feathers
  for feather in map:get_entities("mercuris_feather") do
    feather:remove()
  end
end


-- Set the animation of both Mercuris and his shield
function enemy:set_animation(name)

  shield:set_animation(name)
  sprite:set_animation(name)
end

-- Set shield enabled or not (visually and link can hurt Mercuris)
function enemy:set_shield_enabled(enabled)

  if enabled == true then
    enemy:set_attack_consequence("sword", "protected")
    enemy:set_attack_consequence("explosion", "protected")
    shield:set_opacity(255)
  else
    enemy:set_default_attack_consequences()
    shield:set_opacity(0)
  end
end

-- Intro fall sequence
-- on_finished will be call when the sequence is finished
function enemy:fall_from_ceiling(on_finished)

  sol.audio.play_sound("jump")
  enemy:set_animation("falling")

  -- Mercuris target movement in front of the chest
  movement = sol.movement.create("target")
  movement:set_target(624, 117)
  movement:set_speed(300)
  movement:set_ignore_obstacles(true)
  movement:start(enemy)

  -- Link make an avoiding movement when Mercuris falls
  hero:freeze()
  hero:get_sprite():set_animation("hurt")
  local link_mov = sol.movement.create("target")
  link_mov:set_target(624, 157)
  link_mov:set_speed(200)
  link_mov:start(hero)
  link_mov.on_finished = function()
    link_mov:stop()
    hero:get_sprite():set_animation("stopped")
  end

  -- Falling finished, make a guard pose for 1 second
  movement.on_finished = function()
    sol.audio.play_sound("hero_lands")

    enemy:stop_movement()
    enemy:guard()

    sol.timer.start(enemy, 1000, on_finished)
  end
end

function enemy:guard()

  enemy:set_shield_enabled(true)
  enemy:set_animation("guard")
end

-- First attack is always a dash
function enemy:do_first_attack()

  enemy:run_to_hero_x(function()
    enemy:do_dash_attack()  
  end)
end

-- Decide randomly what will be the next attack
-- Three possible attacks: dash, punch and feathers
function enemy:do_random_next_attack()

  enemy:set_animation("stopped")

  local attack_id = math.random(3)

  if attack_id == 1 then
    -- Jump to the hero
    local hero_x, _ = hero:get_position()
    enemy:jump_back(hero_x, 117, function()
      -- And do a dash
      enemy:run_to_hero_x(function()
        enemy:do_dash_attack()  
      end)
    end)
  elseif attack_id == 2 then
    -- Jump to the hero
    local hero_x, _ = hero:get_position()
    enemy:jump_back(hero_x, 117, function()
      -- And do a punch
      enemy:run_to_hero_x(function()
        enemy:do_punch_attack()  
      end)
    end)
  elseif attack_id == 3 then
    -- Jump on the chest
    enemy:jump_back(624, 85, function()
      -- And throw feathers
      enemy:do_feather_spell_attack()
    end)
  end
end

-- Execute the dash attack
function enemy:do_dash_attack()

  -- Start the positionning_dash phase
  state = "positionning_dash"
  enemy:positionning_dash(function()
    -- Mercuris reaches the back walk
    state = "prepare_dash"
    enemy:prepare_dash_or_punch(function()
      -- Dash is prepared, perform the dash attack
      enemy:dash(function()
        -- Finished: do the next random attack
        enemy:do_random_next_attack()
      end)
    end)
  end)
end

-- Execute the punch attack
function enemy:do_punch_attack(on_finished)

  -- Start the positionning_dash phase
  state = "positionning_dash"
  enemy:positionning_dash(function()
    -- Mercuris reaches the back walk
    state = "prepare_dash"
    enemy:prepare_dash_or_punch(function()
      -- Punch is prepared, perform the punch attack
      enemy:punch(function()
        -- Finished: do the next random attack
        enemy:do_random_next_attack()
      end)
    end)
  end)
end

-- Execute the feather attack
function enemy:do_feather_spell_attack(on_finished)

  local feathers = {}
  local feather_index = 1
  local feather_count = 13
  local feather_next_x = 480
  local feather_next_y = 112
  local feather_placed = 0

  local x, y, layer = enemy:get_position()

  sol.timer.start(enemy, 200, function()
    -- Create a feather
    feathers[feather_index] = map:create_enemy({
      name = "mercuris_feather",
      layer = layer,
      x = x,
      y = y,
      breed = "bosses/mercuris/mercuris_feather",
      direction = 2
    })
    sol.audio.play_sound("boomerang")
    
    -- After 2 seconds of flying in circle, the feathers
    -- is placing forming a line
    local current_feather = feathers[feather_index]
    sol.timer.start(current_feather, 2000, function()
      current_feather:place(feather_next_x, feather_next_y, function()
        feather_placed = feather_placed + 1
        if feather_placed == feather_count then
          -- All feathers are placed, start the dash movement
          for fk, fv in ipairs(feathers) do
            fv:dash()
          end

          -- Next attack
          sol.timer.start(enemy, 500, enemy.do_random_next_attack)
        end
      end)
      feather_next_x = feather_next_x + 24
    end)
    
    feather_index = feather_index + 1

    return feather_index <= feather_count
  end)
end

-- Mercuris is walking back slowly to the back wall
-- while constantly keeping a straight vertical line with Link
-- When he reaches the back wall, he announces a punch
-- on_finished will be called when Mercuris will reach the back wall
function enemy:positionning_dash(on_finished)

  enemy:set_animation("walking")

  -- Move Mercuris up until he reaches the back wall (Y = 77)
  local x, y = enemy:get_position()
  back_walk_y = y
  sol.timer.start(enemy, 50, function()
    back_walk_y = back_walk_y - 1
    if back_walk_y <= 77 then
      on_finished()
    end
    return back_walk_y > 77
  end)  
end

-- Mercuris is preparing a dash or punch for half a second to let the
-- player have time to dodge the attack
-- on_finished will be called when Mercuris is ready to dash or punch
function enemy:prepare_dash_or_punch(on_finished)

  enemy:set_animation("prepare_dash")
  sol.timer.start(enemy, 500, on_finished)
end

-- Actually performing the dash attack
-- Mercuris is dashing to the bottom of the room
-- Then he will stop and stand at the wall
function enemy:dash(on_finished)

  sol.audio.play_sound("cane")
  enemy:set_animation("punch_dash")

  movement = sol.movement.create("straight")
  movement:set_angle(3 * math.pi / 2)
  movement:set_speed(200)
  movement:start(enemy)
  movement.on_obstacle_reached = function()
    movement:stop()
    enemy:set_animation("stopped")
    sol.timer.start(enemy, 500, on_finished)
  end
end

-- Actually perform the punch attack
-- Mercuris is dashing to Link and punching him
-- Then he will stop and stand at the wall
function enemy:punch(on_finished)

  sol.audio.play_sound("cane")
  enemy:set_animation("punch_dash")

  -- Punch movement
  movement = sol.movement.create("straight")
  movement:set_angle(3 * math.pi / 2)
  movement:set_speed(200)
  movement:set_ignore_obstacles(true)
  movement:start(enemy)
  movement.on_position_changed = function()
    local x, y = enemy:get_position()
    local hero_x, hero_y = hero:get_position()
    
    -- Mercuris reaches the hero and will try to punch him
    if y >= hero_y - 20 then
      movement:stop()
      enemy:set_animation("punch")

      sol.timer.start(enemy, 500, function()
        -- Mercuris finishes his attack after making two punches
        on_finished()
      end)

      sol.timer.start(enemy, 100, function()
        -- If in range of punches
        if hero:overlaps(enemy, "sprite", hero:get_sprite(), sprite) then
          -- Throw link to the bottom wall 
          hero:freeze()
          hero:get_sprite():set_animation("hurt")
          hero:get_sprite():set_direction(1)

          local throw_mov = sol.movement.create("straight")
          throw_mov:set_angle(3 * math.pi / 2)
          throw_mov:set_speed(256)
          throw_mov:start(hero)
          throw_mov.on_obstacle_reached = function()
            throw_mov:stop()
            sol.audio.play_sound("running_obstacle")
            hero:start_hurt(2)
          end
        end
      end)
    end
  end
end

-- Call this function to make Mercuris jump back to Link
-- Then he will decide the randomly the next attack
function enemy:jump_back(target_x, target_y, on_arrived)

  enemy:set_can_attack(false)
  enemy:set_animation("guard")
  sol.audio.play_sound("throw")
  state = "jumping"

  -- Wait a little before jumping (frame of the animation)
  sol.timer.start(enemy, 200, function()
  
    -- Start the jump movement
    movement = sol.movement.create("target")
    movement:set_target(target_x, target_y)
    movement:set_speed(192)
    movement:start(enemy)
    
    jump_delta = jump_height + 1
    sol.timer.start(enemy, 16, function()
      
      -- Compute delta across time
      if jump_delta > -jump_height then
        jump_delta = jump_delta - 0.25
      elseif jump_delta < -jump_height then
        jump_delta = -jump_height
      else
        -- Jump "movement" is finished
        state = "standing"
      end

      if state ~= "standing" then
        -- Apply delta to velocity
        jump_velocity = jump_velocity - jump_delta
      else
        -- Force velocity to 0 to be sure shadow and sprite are aligned
        jump_velocity = 0

        -- Stop movement
        movement:stop()
        sol.audio.play_sound("hero_lands")
        enemy:set_animation("stopped")

        -- Wait a bit before next attack
        enemy:set_can_attack(true)
        sol.timer.start(enemy, 500, on_arrived)
      end

      -- Update the sprite position in the enemy
      sprite:set_xy(0, jump_velocity)
      shield:set_xy(0, jump_velocity)

      return state == "jumping"
    end)
  end)
end

-- Quickly run along the X axis to match the hero X coordinate
function enemy:run_to_hero_x(on_reached)

  local reached = false
  local speed = 2
  enemy:set_animation("walking")

  sol.timer.start(enemy, 16, function()
    local hero_x, hero_y = hero:get_position()
    local pos_x, pos_y = enemy:get_position()

    if hero_x < pos_x - 1 then
      pos_x = pos_x - speed
    elseif hero_x > pos_x + 1 then
      pos_x = pos_x + speed
    else
      reached = true
      on_reached()
    end

    enemy:set_position(pos_x, pos_y)

    return reached == false
  end)
end

-- 
function enemy:leave_with_chest()

  hero:freeze()
  sol.timer.start(enemy, 1500, function()
    shield:set_opacity(0)
    sprite:set_animation("stopped")
    game:start_dialog("dungeon_1.boss.defeated", function()
      enemy:jump_back(624, 85, function()
        sprite:set_animation("guard")
        sol.timer.start(enemy, 300, function()
          local heart_container = map:get_entity("heart_container")
          if heart_container ~= nil then
            heart_container:set_enabled(true)
          end
          local chest_blocker = map:get_entity("chest_blocker")
          if chest_blocker ~= nil then
            chest_blocker:set_enabled(false)
          end
  
          sol.audio.play_sound("sword_spin_attack_release")
          sol.audio.play_music("agahnim")
  
          game:set_value("dungeon_1_boss", true)
  
          local leave_mov = sol.movement.create("straight")
          leave_mov:set_angle(math.pi / 2)
          leave_mov:set_speed(260)
          leave_mov:set_max_distance(300)
          leave_mov:set_ignore_obstacles(true)
          leave_mov:start(enemy)
          leave_mov.on_finished = function()
            enemy:remove()
            hero:unfreeze()
          end
  
          local chest = map:get_entity("legendary_chest")
          local chest_mov = sol.movement.create("straight")
          chest_mov:set_angle(math.pi / 2)
          chest_mov:set_speed(260)
          chest_mov:set_max_distance(300)
          chest_mov:set_ignore_obstacles(true)
          chest_mov:start(chest)
          chest_mov.on_finished = function()
            chest:remove()
          end
        end)
      end)
    end)
  end)
end